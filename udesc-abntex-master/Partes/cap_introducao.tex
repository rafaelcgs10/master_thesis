% !TEX root = ../Principal.tex
\chapter{Introdução}
Sistemas de tipos são regras que ditam quais expressões podem ser
associadas a quais tipos. Em sistemas formais, como o Cálculo Lambda,
servem para evitar paradoxos e computações infinitas. Nas primeiras
linguagens de programação, sistemas de tipos apenas classificavam os
dados para serem tratados corretamente pelo processador, garantindo o
sentido operacional do programa. Com o avanço das linguagens, passaram
a ser utilizados como regras para identificar falhas na consistência
de programas. Este tipo de verificação expõem não só falhas triviais
na lógica do programador, mas também profundos erros conceituais
\cite{pi:02}.

Dentro das linguagens de programação, aquelas que utilizam tipos como
uma forma de verificação são classificadas como de tipagem
estática. Pois, uma vez que uma variável ou dado é associado a um
tipo, esta associação se mantém fixa ao longo de todo o programa.
Quando o tipo de uma variável pode mudar ao logo do programa, por
exemplo como em Python , a linguagem é classificada como de tipagem
dinâmica.  A tipagem dinâmica, por algumas pessoas, é vista como
vantajosa por ser mais flexível, permissível e mais produtiva, já que
o programador não precisa assinar os tipos das variáveis ou se quer
fazer qualquer tipo de verificação mental sobre os tipos do seu
programa. Porém essa mesma flexibilidade permite situações com grande
potencial de desastre, como uma lista em Python com dados de variados
tipos: \mintinline{python}{[1, 'a', objeto, [2]]}.

Mesmo nas linguagens de tipagem estática a responsabilidade do
programador de assinar os tipos pode ser opcional. Para isso é
necessário que o compilador seja capaz de não só verificar os tipos do
programa, mas como também fazer a inferência dos mesmos.  A inferência
de tipos é o processo de encontrar a assinatura de tipo mais geral de
um programa, chamada de tipo principal, aquela que melhorar
representa todos os possíveis usos do programa. O proposito da
inferência de tipos em linguagens de programação é dar a liberdade ao
programador de escolher anotar ou não os tipos das funções. Isso é
particularmente interessante quando uma função tem uma assinatura
muito complexa ou quando é difícil pensar no tipo mais geral da mesma,
visto que escrever um tipo mais especializado limita o seu uso.

Em linguagens com polimorfismo paramétrico, como
C++, Java e Haskell, o tipo mais geral pode ser dado pela
parametrização do tipo de um dado.  Por exemplo, em Haskell a função
que conta a quantidade de elementos numa lista tem o tipo mais geral
\mintinline{haskell}{[a] -> Int}\footnote{Leia-se recebe uma lista de
  valores de tipo \mintinline{haskell}{a} e retorna um inteiro.}, pois
o tipo dos dados que estão na lista não são relevantes para a
contagem. Portanto, esse programa pode ser aplicado a listas com
quaisquer tipos de dados, pois a variável de tipo
\mintinline{haskell}{a} é um parâmetro que pode ser trocado por
qualquer outro tipo.

Nem todos os programas são bem tipados, isso é, existem programas que
são mal formados e que não tem uma assinatura de tipos, como um
programa que soma um número inteiro com um dado do tipo lista. Esses
programas não podem ser aceitos como válidos pela
inferência/verificação de tipos e devem ser descartados/rejeitados
devido ao erro de tipo presente neles. Por outro lado, devido às
limitações dos algoritmos de inferência, alguns programas bem tipados
podem ser rejeitados. Esses falsos negativos são preferíveis a aceitar
programas mal tipados, por isso sempre adotam-se abordagens mais
conservadoras na inferência. Um dos objetivos das pesquisas em
sistemas de tipos para linguagens de programação é expandir a
inferência de tipos de programas bem tipados.

Uma das situações onde ocorre falsos negativos na inferência de tipos
é na recursão polimórfica. Haskell rejeita programas com recursão
polimórfica se a assinatura de tipo não for explicitamente anotada.
Essa dificuldade se deve ao fato de que inferir tipos nesse caso é
indecidível.  O que significa que não há algoritmo total e completo
capaz de tratar todos os programas. Na prática, duas abordagens são
encontradas: obrigar a assinatura de tipos como em Haslkell ou
utilizar um limite de iteração arbitrário no algoritmo de inferência
como em Mercury.

A recursão polimórfica acontece quando o tipo da chamada recursiva de
uma função pode mudar ao longo das recursões, em geral, isso ocorre em
\textit{nested} (também chamado de não-uniforme ou não-regular)
\textit{data types} e funções com recursão mútua. Por exemplo,
\cite{ok:98} propôs árvores binárias balanceadas representadas pelo
tipo:
\begin{minted}{haskell}
  data Seq a = Nil | Cons a (Seq (a,a)).
\end{minted}
A parte recursiva do tipo, \mintinline{haskell}{Seq (a, a)}, é
diferente de \mintinline{haskell}{Seq a}, caracterizando um tipo
não-uniforme.  Como é impossível construir uma árvore não balanceada
com este tipo de dado, então a função \mintinline{haskell}{len}, que
está descrita na Figura \ref{fig_len}, e que computa o número de
elementos da árvore pode ser processada com tempo de execução
$\mathcal{O}(\log{}n)$, onde $n$ é o número de elementos.

\begin{figure}[htb]
  \caption{\label{fig_len}Exemplo de função com recursão polimórfica.}
\begin{minted}{haskell}
  len :: Seq a -> Int
  len Nil = 0
  len (Cons x s) = 1 + 2 * (len s)
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

Outro exemplo que demostra a utilidade da recursão polimórfica foi
apresentado por \cite{bi:98}, no qual tipos não-uniformes são
utilizados para representar termos do Cálculo Lambda na notação
\textit{De Bruijn}. Vários outros exemplos precisando de recursão
polimórfica foram apresentados \cite{co:95, ok:97, ha:05, bi:98,
  ma:08} na literatura, portando há uma considerável aplicabilidade
desta forma de definição de função.

Na literatura há algumas abordagens que tentam contornar a dificuldade
de inferir tipos na presença de recursão polimórfica, mas essas
propostas geralmente são bastantes restritivas, pois são por meio de
\textit{intersection types}\footnote{Detalhado no Capítulo
  Fundamentos.}. Por outro lado, o
semi-algoritmo\footnote{Algoritmos que não garantem parar em todas as
  entradas. Também podem ser chamados de algoritmos de semi-decisão.}
MMo fornecido por \cite{va:03} não depende de \textit{intersection
  types} e consegue tipar diversos casos de recursão
polimórfica. Infelizmente, MMo não tem as suas propriedades
fundamentais provadas. Tem-se como conjectura a sua terminação
(totalidade) e a sua consistência (correto). Este semi-algoritmo foi
testado em diversos casos e nunca foi encontrado um caso de não
terminação.

Outro problema de inferência considerado difícil acontece na presença
de recursão mútua, o qual acontece quando duas definições são
inter-dependentes.  Neste caso, Haskell ao menos pode fazer uma
ordenação topológica no processo de inferência. Considere um contexto
onde a função \mintinline{haskell}{const} tem tipo
\mintinline{haskell}{a -> b -> a}, então o tipo das funções 
\begin{minted}{haskell}
  f x = const x g
  g x = f 'A',
\end{minted}
é inferido como se a declaração fosse:
\begin{minted}{haskell}
  f x = let g y = f 'A' in const x g.
\end{minted}
A função \mintinline{haskell}{f} original, que poderia ter seu tipo
polimórfico anotado como \mintinline{haskell}{a -> a}, é tratada como
monomórfica como se ocorresse no escopo da sua própria definição,
sendo inferido o tipo \mintinline{haskell}{Char -> Char}. Este é um
pequeno exemplo que demostra que Haskell não tem tipo principal. Em
testes de mesa do MMo do Capítulo \ref{cap_mmo}, foi possível inferir o tipo mais geral para
esse exemplo. Assim, parece que MMo é capaz de tratar de maneira mais
geral tanto recursão polimórfica quanto mútua.

O objetivo geral desta dissertação é investigar se o semi-algoritmo
MMo tem as propriedades de totalidade e correção em relação ao seu
sistema de tipos (especificação), que também é um elemento da investigação.
Se necessário, propõem-se modificações no semi-algoritmo para que as
propriedades possam ser provadas. O método utilizado é a formalização
do MMo e a sua especificação no assistente de provas Coq, no
qual tentar-se-á efetuar as provas de ambas propriedades.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
