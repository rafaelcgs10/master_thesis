% !TEX root = ../Principal.tex
\chapter{Semi-algoritmo MMo}
\label{cap_mmo}
Este capítulo dedica-se a apresentar algumas definições preliminares
para o MMo, explicar de maneira intuitiva o seu funcionamento, alguns
exemplos de inferência com recursão polimórfica e mútua e, por fim, a
definição (abstrata) do semi-algoritmo.

A gramática informal na Figura \ref{fig_grama} é uma extensão dos
termos e tipos de Damas-Milner. Adicionam-se construtores de tipos e de
dados, índices (números naturais) às variáveis, e expressões
\textit{let} permitem grupos de definições, chamados de
\textit{BindGroup} em \cite{va:03}. Nota-se que este enriquecimento
da linguagem é importante para ser possível expressar exemplos interessantes
de recursão polimórfica 

A notação
\mintinline{haskell}{[i]} significa uma sequência finita de índices
únicos $i$, que quando vazia pode ser omitida (análogo para $[p]$). Por
simplicidade pode-se representar $[i]$ apenas com o símbolo $s$ e a
inserção de um novo índice $i$ em $s$ é denotada por $i::s$. As notações
\mintinline{haskell}{{a}} e \mintinline{haskell}{{b}}
significam, respectivamente, sequências finitas e não vazias de alternativas
e definições (\textit{binds}). Os padrões são separados por espaços, as sequências
de índices por vírgulas, as alternativas são separadas por \textit{pipes} (símbolo $|$)
e as definições são separadas por ponto e vírgulas (símbolo \mintinline{haskell}{;}).

\begin{figure}[H]
\caption{\label{fig_grama}Gramática informal das expressões e tipos.}
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
*Variáveis*             x
*Construtor de dado*    c
*Padrão*                p *::=* x | c [p] 
*Alternativa*           a *::=* x [p] = e;
*Grupo*                 g *::=* {e}
*Expressões*            e *::=* x
                        | e e'
                        | *$\lambda$*x.e
                        | *\textbf{case}* e *\textbf{of}* a
                        | *\textbf{let}* g *\textbf{in}* e

*Construtor de tipo*    *C*
*Índices *              i
*Variáveis de tipo*     *$\alpha^{[i]}$*
*Tipos*                 *$\tau$* *::=* *$\tau \rightarrow \tau'$* | *$\tau \: \tau'$* | *$\alpha^{[i]}$* | *C*
*Schemes*               *$\sigma$* *::=* *$\forall \alpha . \sigma$* | *$\tau$*
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

A substituição é definida como em Damas-Milner: um finito mapeamento
de variáveis de tipo para tipos, com a ressalva que para duas
quaisquer variáveis de tipo $\alpha_1^{s_1}$ e $\alpha_2^{s_2}$ serem
a mesma é necessário $\alpha_1 = \alpha_2$ e $s_1 = s_2$.  O algoritmo
de unificação também não sofre mudanças significativas, apenas
adicionam-se os casos para os novos tipos.

O MMo funciona de maneira \textit{botton-up} assim como o algoritmo
\textit{W}, ou seja, primeiro inferem-se os termos mais internos da
expressão para seguir inferindo os mais externos.
Por outro lado, existem algumas diferenças essenciais:

\begin{enumerate}
\item As variáveis contam com uma sequência de índices que servem para
  a detecção de dependências circulares em substituições.
\item Um contexto $\Gamma$ é um conjunto de triplas (\textit{variável},
  $Kind\_def$, \textit{scheme}), onde $Kind\_def$\footnote{Por simplicidade omite-se esta informação em alguns exemplos.}
  distingue se uma variável refere-se a uma abstração $\lambda$ ou uma definição
  \mintinline{haskell}{let}. Não há condição de
    consistência, ou seja, uma variável pode ocorrer diversas vezes com
    diferentes tipos. Essa forma especial de contexto é chamada de
    \textit{typing context} (ou apenas contexto).
\item O algoritmo \textit{W} tem como retorno uma dupla com o tipo
  simples inferido $\tau$ e uma substituição $\mathbb{S}$, já o MMo retorna uma
  dupla com o tipo simples inferido $\tau$ e um contexto $\Gamma$.
  Essa dupla é chamada de \textit{contextualized simple
    type} e a quantificação de $\tau$ sobre o respectivo contexto é  
  chamada de \textit{principal typing} (tipagem principal).
\item Para cada definição num \textit{BindGroup} infere-se o respectivo
  \textit{contextualized simple type}.
  O conjunto com os pares de variáveis
  (identificadores das definições do \mintinline{haskell}{let}) e
  os seus respectivos \textit{contextualized simple type} é chamado de
  inferidos e requeridos, e representado pelo símbolo $\Sigma$.
\end{enumerate}

\textit{Principal typing} é um dos ingredientes principais do MMo,
ainda que não utilizado diretamente no mesmo, e é consequência da
ideia de \cite{ji:96}, o qual sugeriu \textit{principal typing} para a
solução do problema da inferência de definições mutuamente
recursivas. Uma solução de tipagem principal $(\tau, \Gamma)$ para um
problema $(e, \Gamma_0)$ é tal que $\sigma$ fornece mais e $\Gamma$
requer menos que qualquer outra solução.  Tipagem principal não deve
ser confundida com tipo principal, conforme explica a Tabela \ref{tab_tipagem}.

\begin{table}[ht!]
  \caption{\label{tab_tipagem}Distinção entre tipo principal e tipagem
    principal.}
  \begin{tabular}{|l|l}
    \cline{1-1}
    \textbf{Principal type}    &                                                                                                                                                                                                                                                                  \\ \hline
    Fornecido:        & \multicolumn{1}{l|}{Um termo $e$ e um contexto $\Gamma$.}                                                                                                                                                                                                        \\ \hline
    Existe:           & \multicolumn{1}{l|}{Um tipo $\sigma$ que representa os possíveis tipos de $e$ para $\Gamma$.}                                                                                                                                                                  \\ \hline
    \textbf{Principal typing} &                                                                                                                                                                                                                                                                  \\ \hline
    Fornecido:         & \multicolumn{1}{l|}{Um termo $e$ e um contexto inicial $\Gamma_0$}                                                                                                                                                                                                                                \\ \hline
    Existe:           & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Um tipo $\sigma$ e um contexto $\Gamma$, tais que $\Gamma_0 \subset \Gamma$, $\Gamma$ é\\ estritamente o menor conjunto necessário para inferir o tipo de\\ $e$ e $\sigma$ representa todos os possíveis de $e$ para $\Gamma$.\end{tabular}} \\ \hline
  \end{tabular}
  \legend{Fonte: o autor. Adaptado de \cite{va:04}}
\end{table}

\section{Inferência de grupos de definições}
O ponto crucial do algoritmo MMo é inferir o tipo de um grupo de
definições de expressões \mintinline{haskell}{let}, pois é neste momento
que a recursão polimórfica e mútua são tratadas. Para isso, o
primeiro passo é descobrir o conjunto $\Sigma$ com todos os tipos
inferidos e requeridos do \mintinline{haskell}{let}.  Então, gera-se a
partir de $\Sigma$ um conjunto $\Omega$ de pares tipos inferidos (instâncias) e requeridos 
$(\tau', \tau)$ que precisam ser unificados: o tipo de cada suposição
\mintinline[escapeinside=**,mathescape=true]{haskell}{x:*$\tau$*} em
cada um dos $\Gamma$ em $\Sigma$ precisa ser unificado com uma instância do seu
respectivo tipo inferido $\tau'$ para \mintinline[escapeinside=**,mathescape=true]{haskell}{x}
(caso exista). Essa instância é computada pela função \mintinline{haskell}{supInst}, a
qual troca cada variável de tipo $\alpha^{s}$ de $\tau'$ por uma nova
instância $\alpha^{i::s}$, mantendo o $\alpha$ e inserindo um novo
índice $i$ em $s$, desde que $\alpha^{s}$ não ocorra numa suposição de
abstração $\lambda$ do seu respectivo $\Gamma$.

O próximo passo é verificar a existência de dependências circulares na
substituição $\mathbb{S}$ gerada pela unificação de $\Omega$.  As
instâncias geradas pela função \mintinline{haskell}{supInst} preservam
o $\alpha$ e os antigos índices, o que funciona como uma memória da origem
da instância. Uma dependência circular ocorre quando uma substituição
troca uma variável de tipo $\alpha^s$ por um tipo $\tau$, o qual ocorre uma
variável de tipo $\alpha^{s'}$ tal que $s$ seja uma subsequência de
$s'$ (representado por $s|s'$). Ou seja, quando uma substituição troca
uma variável de tipo por um tipo contendo uma variável de tipo que é sua
instância. Por exemplo, uma substituição
\mintinline[escapeinside=**,mathescape=true]{haskell}{[a*$^{1, 0} \rightarrow$* b/a*$^0$*]}
tem dependência circular, pois $0|1,0$.

Caso exista dependência circular em $\mathbb{S}$, então o MMo termina
com uma mensagem de erro. Caso contrário, então aplica-se a
substituição $\mathbb{S}$ nos tipos inferidos e requeridos $\Sigma$, o
que resulta em $\Sigma'$. Se não existir diferença entre $\Sigma$ e
$\Sigma'$ (modulo renomeação de variáveis de tipo), então a inferência
do grupo termina retornando $\Sigma$. Uma substituição é uma
renomeação se for da forma $[\alpha'_i/\alpha_i]$ com $\alpha_i$
distintos. Se não, repetem-se os passos anteriores com o $\Sigma'$.

\subsection{Exemplos}
Como exemplo inicial da inferência em recursão polimórfica realizada
pelo algoritmo MMo, considere o programa \mintinline{haskell}{len} da
Figura \ref{fig_len}. O seu conjunto inicial $\Sigma$  de
inferidos e requeridos é: 
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(Nil,(Seq a, *$\emptyset$*)),
  (Cons,(b *$\rightarrow$* Seq (b, b) *$\rightarrow$* Seq b, *$\emptyset$*)),
  (len, (Seq c *$\rightarrow$* Int, *$\{$*(+):Int *$\rightarrow$* Int *$\rightarrow$* Int, len:Seq(c, c) *$\rightarrow$* Int*$\}$*))*$\}$*,
\end{minted}
do qual computa-se o conjunto $\Omega$ de tipos a serem unificados:
\begin{minted}[escapeinside=**,mathescape=true]{haskell}
  *$\{$*(Seq c*$^0$**$\rightarrow$* Int, Seq(c, c) *$\rightarrow$* Int)*$\}$*,
\end{minted}
tal que \mintinline[escapeinside=**,mathescape=true]{haskell}{c*$^0$*} é uma
variável de tipo \textit{fresh} criada por
\mintinline{haskell}{supInst} a partir de \mintinline{haskell}{c}.
Unificar o primeiro elemento com o segundo de cada dupla no conjunto
$\Omega$ apenas gera uma substituição
\mintinline[escapeinside=**,mathescape=true]{haskell}{[(c, c)/c*$^0$*]},
que não tem dependência circular e não modifica os tipos de
$\Sigma$ (ignorando renomeações de
variáveis). Por fim, esses tipos são utilizados como estão para
finalizar o processo de inferência.

Uma substituição que modifica os tipos inferidos e requiridos 
acontece no simples caso da função:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  h x =(g x) + 1
  g x = h (g x)
\end{minted}
tal que o $\Sigma$ computado é:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(h,(c *$\rightarrow$* Int, *$\{$*(+):Int *$\rightarrow$* Int *$\rightarrow$* Int, g:c *$\rightarrow$* Int*$\}$*)),
   (g,(a *$\rightarrow$* b,*$\{$*g:a *$\rightarrow$* d, h: d *$\rightarrow$* b*$\}$*))*$\}$*,
\end{minted}
cujo respectivo $\Omega$ é:
\begin{minted}[escapeinside=**,mathescape=true]{haskell}
  *$\{$*(c*$^0$* *$\rightarrow$* Int, d *$\rightarrow$* b), ( a*$^1$* *$\rightarrow$* b*$^1$*, a *$\rightarrow$* d), (a*$^2$* *$\rightarrow$* b*$^2$*, c *$\rightarrow$* Int)*$\}$*,
\end{minted}
e cuja substituição gerada pela unificação dos pares é
\mintinline{haskell}{[Int/b, Int/d]} que não tem dependência circular,
porém modifica os tipos de $\Sigma$, portanto este processo
deve se repetir para o novo $\Sigma'$:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(h,(c *$\rightarrow$* Int, *$\{$*(+):Int *$\rightarrow$* Int *$\rightarrow$* Int, g:c *$\rightarrow$* Int*$\}$*)),
   (g,(a *$\rightarrow$* Int,*$\{$*g:a *$\rightarrow$* d, h: Int *$\rightarrow$* Int*$\}$*))*$\}$*,
\end{minted}
tal que o seu respectivo $\Omega'$ é:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*( a*$^1$* *$\rightarrow$* Int, a *$\rightarrow$* Int), (a*$^1$* *$\rightarrow$* Int, c *$\rightarrow$* Int)*$\}$*
\end{minted}
e a unificação destes pares gera uma substituição sem dependências
circulares e que não modifica $\Sigma'$.

O exemplo de recursão mútua da introdução
\begin{minted}{haskell}
  f x = const x g
  g x = f 'A',
\end{minted}
o qual Haskell inferiu um tipo não principal,
tem o conjunto $\Sigma$:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(f,(a *$\rightarrow$* a, *$\{$*const:a *$\rightarrow$* c *$\rightarrow$* a, g:c*$\}$*)),
   (g,(d *$\rightarrow$* e,*$\{$*f:Char *$\rightarrow$* e*$\}$*))*$\}$*,
\end{minted}
tal que os tipos a serem unificados são o $\Omega$:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(a*$^0$* *$\rightarrow$* a*$^0$*, Char *$\rightarrow$* e), (d*$^1$* *$\rightarrow$* e*$^1$*, c)*$\}$*,
\end{minted}
os quais geram a substituição
\mintinline[escapeinside=**,mathescape=true]{haskell}{[Char/a*$^0$*, Char/e, d*$^1$* *$\rightarrow$* e*$^1$*/c]},
modificando os inferidos e requeridos para o $\Sigma'$:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(f,(a *$\rightarrow$* a, *$\{$*const:a *$\rightarrow$* (d*$^1$* *$\rightarrow$* e*$^1$*) *$\rightarrow$* a, g:d*$^1$* *$\rightarrow$* e*$^1$**$\}$*)),
   (g,(d *$\rightarrow$* Char,*$\{$*f:Char *$\rightarrow$* Char*$\}$*))*$\}$*,
\end{minted}
cujo $\Omega'$ é:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(a*$^2$* *$\rightarrow$* a*$^2$*, Char *$\rightarrow$* Char), (d*$^2$* *$\rightarrow$* Char, d*$^1$* *$\rightarrow$* e*$^1$*)*$\}$*,
\end{minted}
gerando a nova substituição 
\mintinline[escapeinside=**,mathescape=true]{haskell}{[Char/a*$^2$*, Char/e*$^1$*]}.
Os tipos inferidos e requeridos são modificados para:
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  *$\{$*(f,(a *$\rightarrow$* a, *$\{$*const:a *$\rightarrow$* (d*$^1$* *$\rightarrow$* Char) *$\rightarrow$* a, g:d*$^1$* *$\rightarrow$* Char*$\}$*)),
   (g,(d *$\rightarrow$* Char,*$\{$*f:Char *$\rightarrow$* Char*$\}$*))*$\}$*,
\end{minted}
cujos tipos a serem unificados geram uma substituição que não causa
mais modificações ao mesmo.

O menor caso com dependência circular na substituição gerada pelos
tipos inferidos e requeridos acontece na função
\mintinline[escapeinside=**,mathescape=true]{haskell}{f x = f},
cujos inferidos e requeridos a serem unificados são 
\mintinline[escapeinside=**,mathescape=true]{haskell}{*$\{$*(a*$^0$* *$\rightarrow$* b*$^0$*, b*$\}$*,}.
A unificação destes pares gera uma substituição
\mintinline[escapeinside=**,mathescape=true]{haskell}{[a*$^0$* *$\rightarrow$* b*$^0$*/b]},
que contem uma dependência circular direta, pois 
\mintinline[escapeinside=**,mathescape=true]{haskell}{[b*$^0$*]}
originou de \mintinline{haskell}{b}.

\section{Abstração do MMo}
A ideia do algoritmo e os exemplos acima servem como base para uma
representação abstrata do MMo na Figura \ref{fig_grupo}. A função
\mintinline{haskell}{inferG} tem como entrada um contexto inicial
$\Gamma$ e um grupo de definições \mintinline{haskell}{g} de um
\mintinline{haskell}{let}. O $\Sigma$ é computado para então ser
passado para a função
\mintinline[escapeinside=**,mathescape=true]{haskell}{unifyG},
a qual calcula o conjunto $\Omega$ e, então, chama-se a função
\mintinline{haskell}{semi-unify} para realizar a unificação dos pares
em $\Omega$.

A unificação de $\Omega$ acontece unificando o primeiro elemento com o
segundo de cada uma das duplas e compondo os resultados.  Ao final,
verifica-se a existência de dependências circulares por meio da função
\mintinline{haskell}{checkCircular}\footnote{Melhor explicado na
  subseção \ref{sec_circ}.}. Esta representação abstrata considera uma
computação monádica com estados e exceções, assim uma falha em alguma
unificação ou a existência de dependência circular causa o algoritmo
parar com uma mensagem de erro.

A substituição $\mathbb{S}$ retornada pela função
\mintinline{haskell}{semi-unify} é aplicada nos tipos inferidos e
requeridos de $\Sigma$. Caso $\mathbb{S}$ não modifique $\Sigma$, modulo
renomeação, então
\mintinline[escapeinside=**,mathescape=true]{haskell}{unifyG}
para retornando $\Sigma$, ou então acontece uma chamada recursiva de
\mintinline[escapeinside=**,mathescape=true]{haskell}{unifyG}
com o novo $\Sigma'$. Observa-se que esta recursão não é primitiva e não é
óbvio se existe uma relação bem-fundada para ser utilizado como
critério de parada.

\begin{figure}[h!]
 \caption{\label{fig_grupo}Inferência de grupos de definições.}
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  inferG*$(\Gamma$*, g*$)$* =
       *$\Sigma$* *$\leftarrow$* *$\{($*x*$_i$:$\tau_i$, $\Gamma_i)$ $|$* *$($*x*$_i$*, e*$_i)$* *$\in$* g, MMo*$(\Gamma$*, e*$_i)$* = *$(\tau_i, \Gamma_i)\}$*
       *$\Sigma'$* *$\leftarrow$* unifyG*$(\Sigma)$*
       retorna *$\Sigma'$*

  unifyG*$(\Sigma)$* =
       *$\Gamma_{\Sigma}$* *$\leftarrow$* *$\bigcup_{i..|\Sigma|} \Gamma_i$* excluindo suposições referentes a abstrações *$\lambda$* 
       *$\Omega$* *$\leftarrow$* *$\{(\tau_i, \tau_j')$ $|$* *$($*x*$_i$*, *$\tau_i) \in \Gamma_{\Sigma}$*, *$\Sigma(x_i)$* = *$\tau_j$*, supInst*$(\tau_j)$* = *$\tau_j'\}$*
       *$\mathbb{S}$* *$\leftarrow$* semi-unify*$(\Omega)$*
       *$\Sigma'$* *$\leftarrow$* *$\mathbb{S}\Sigma$*
       se *$\Sigma$* *$\equiv_r$* *$\Sigma'$* então retorna *$\Sigma$* senão unifyG*$(\Sigma')$*
  
  semi-unify*$(\Omega)$* =
       *$\mathbb{S}$* *$\leftarrow$* unify*$(\Omega)$*
       checkCircular*$(\mathbb{S})$*
       retorna *$\mathbb{S}$*

\end{minted}
\legend{Fonte: o autor. Adaptado de \cite{va:03}.}
\end{figure}

A renomeação feita pela função \mintinline{haskell}{supInst} corresponde
à criação de novas variáveis no lado esquerdo das inequações do problema
da semi-unificação, no qual a inequação $t_i \leq_i t'$ é equivalente ao uso
polimórfico de uma variável em $t_i$ como $t'$. 

A parte do MMo que faz a inferência de uma expressão completa ainda
não foi representada de maneira abstrata, visto a complexidade, e
é parte da continuidade deste trabalho. Em particular, a quantificação
de variáveis de tipo precisa ser feita de maneira diferente em relação ao
algoritmo \textit{W}, pois a introdução de variáveis no contexto não acontece
na abstração $\lambda$ e no \textit{let} como no algoritmo.

\subsection{Detecção de dependência circular}
\label{sec_circ}
A detecção de dependência circular numa substituição $\mathbb{S}$
ocorre verificando se um elemento $\alpha^s$ do domínio de uma substituição leva
a algum tipo com algum $\alpha^{s'}$, tal que $s|s'$ ($s$ é uma subsequência de $s'$).
A relação de ser subsequência pode ser indireta, portanto é necessário fazer
uma verificação transitiva na substituição. Para isso, aplica-se a função
\mintinline{haskell}{fold} para acumular as verificações passadas (ver Figura \ref{fig_circ}). 

O último exemplo apresentado possui dependência circular direta.
Um caso de indireta seria 
\mintinline[escapeinside=**,mathescape=true]{haskell}{[c/b, a *$\rightarrow$* b*$^{1,0}$*/c]},

\begin{figure}[h!]
 \caption{\label{fig_circ}Verificação de dependência circular.}
\begin{minted}[escapeinside=**,mathescape=true]{haskell} 
  checkCircular*$(\mathbb{S})$* = se fst *$\circ$* fold circ *$($*Falso, []*$)$* então falha
         senão retorna nada


  circ*$(\_, ($*Verdade, *$\mathbb{S}))$* = *$($*Verdade, *$\mathbb{S})$*
  circ*$((\tau, \alpha^{s}), ($*False, *$\mathbb{S}))$* = se checkS*$(\alpha^{s},$*apply*$(\mathbb{S}, \tau))$* então *$($*Verdade, *$\mathbb{S})$* 
                          senão *$($*Falso, *$[$*apply*$(\mathbb{S}, \tau))/\alpha^s] \circ \mathbb{S})$*

  apply*$(\mathbb{S}$*, *$\tau \rightarrow \tau')$* = (apply*$(\mathbb{S}$*, *$\tau)$*) *$\rightarrow$* (apply*$(\mathbb{S}$*, *$\tau')$*)
  apply*$([\tau_i/\alpha_i^{s_i}]$*, *$\alpha^{x::s})$* = se para algum *$i$* há *$\alpha^{s}$* *$\equiv$* *$\alpha_i^{s_i}$* então *$\tau_i$*
                       senão *$\alpha^{x::s}$*

  checkS*$(\alpha_1^{s_1}$*, *$\tau \rightarrow \tau')$* = checkS*$($*c*$_1$* s*$_1$*, *$\tau)$* *$\vee$* checkS*$($*c*$_1$* s*$_1$*, *$\tau')$*
  checkS*$(\alpha_1^{s_1}$*, *$\alpha_2^{s_2})$* = *$\alpha_1$* *$=$* *$\alpha_2$* *$\wedge$* s*$_1$*|s*$_2$*
  checkS*$($*_, _*$)$* = Falso
       
\end{minted}
\legend{Fonte: o autor. Adaptado de \cite{va:03}.}
\end{figure}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
