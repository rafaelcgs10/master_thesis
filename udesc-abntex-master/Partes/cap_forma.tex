% !TEX root = ../Principal.tex
\chapter{Formalização em Coq}
Este capítulo apresenta uma parcial formalização em Coq do
semi-algoritmo MMo e discute a problemática da terminação e da
consistência do mesmo.  No momento, esta formalização é parcial, com
somente as definições básicas e a implementação (sem provas) do que
seria parte da inferência de grupos de definições. Como apresentado
anteriormente, MMo trata recursão polimórfica e mútua, porém
formalizar as regras de tipagem para recursão mútua (\textit{binding
  groups}) pode trazer complicações além do escopo deste
trabalho. Então, por simplicidade, esta formalização vai se limitar
somente à recursão polimórfica, de maneira que algumas simplificações
no MMo serão realizadas.

A principal fonte de inspiração para formalização do MMo
em Coq vem de um trabalho similar para o algoritmo \textit{W} por
\cite{du:99}. Esta formalização, ainda, sustenta-se no uso da
formalização da unificação feita por \cite{ri:16}, no qual apenas
pequenas adaptações precisaram ser feitas, como adicionar a lista de
índices nos identificadores das variáveis de tipo:
\begin{minted}{coq}
  Definition id := (ty_name * (list ty_index))%type,
\end{minted}
onde \mintinline{coq}{ty_name} e \mintinline{coq}{ty_index} são apenas
números naturais.

Os tipos simples desta formalização são os mesmos utilizados na
unificação de \cite{ri:16} (Figura \ref{fig_tipos_simples}) e as
expressões da Figura \ref{fig_termos} são uma formalização dos de
Damas-Milner (Figura \ref{fig_grama_dm}). Ou seja, por simplicidade
utiliza-se \textit{lets} simples no lugar de \textit{lets} com
\textit{bindgroups} e a aplicação de tipos simples é omitida
($\tau \: \tau'$).  Dessa maneira, a função \mintinline{haskell}{inferG} do
MMo somente precisa considerar uma única definição e não um grupo.

\begin{figure}[ht!]
  \caption{\label{fig_termos}Formalização dos termos em Coq.}
\begin{minted}{coq}
  Inductive term : Type :=
  | var_t   : id_t -> term
  | app_t   : term -> term -> term
  | lam_t   : id_t -> term -> term
  | let_t   : id -> term -> term -> term
  | fix_t   : id_t -> term -> term
  | const_t : term.
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

Os tipos quantificados (\textit{schemes}) são definidos separados dos
simples (ver Figura \ref{fig_schms}), pois isto facilita quando é preciso
lidar estritamente com um deles, como no caso da unificação.
O construtor \mintinline{coq}{schm_gen} representa uma variável de tipo
quantificada, então por exemplo o tipo \mintinline[escapeinside=**,mathescape=true]{haskell}{*$\forall$*a.a *$\rightarrow$* b}
é representado pelo dado
\begin{minted}[escapeinside=**,mathescape=true]{coq}
  schm_arrow (schm_gen (0 nil)) (schm_var (1 nil)),
\end{minted}
onde \mintinline{coq}{nil} é a lista vazia de índices.

\begin{figure}[ht!]
  \caption{\label{fig_schms}Formalização de \textit{schemes} em Coq.}
\begin{minted}{coq}
  Inductive schm : Type :=
  | schm_var : id -> schm
  | schm_con : id -> schm
  | schm_gen : id -> schm
  | schm_arrow : schm -> schm -> schm. 
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

Representar identificadores como números naturais,
ao invés de \textit{strings} como numa tradicional implementação,
torna mais simples lidar com tipos quantificados.
A implementação do algoritmo de inferência não precisa lidar quando
dois tipos quantificados são o mesmo, porém a sua verificação sim.
Dois tipos diferentes \mintinline[escapeinside=**,mathescape=true]{haskell}{a *$\rightarrow$* a}
e \mintinline[escapeinside=**,mathescape=true]{haskell}{b *$\rightarrow$* b} podem
ser quantificados, respectivamente, para \mintinline[escapeinside=**,mathescape=true]{haskell}{*$\forall$*a.a *$\rightarrow$* a}
e \mintinline[escapeinside=**,mathescape=true]{haskell}{*$\forall$*b.b *$\rightarrow$* b}, que são sintaticamente
diferentes, mas representam exatamente o mesmo conjunto de tipos.
Para garantir que dois tipos generalizados sejam sintaticamente iguais, a respeito
da equivalência $\alpha$, utiliza-se a notação \textit{De Bruijn}:
linearmente num \textit{scheme} $\sigma$, uma variável de tipo quantificada é
\mintinline[escapeinside=**,mathescape=true]{haskell}{schm_gen (*$n$* nil)}
se for a $n$-ésima numa leitura da esquerda para a direita de $\sigma$.  
Logo, a generalização do tipo 
\mintinline[escapeinside=**,mathescape=true]{haskell}{b *$\rightarrow$* a *$\rightarrow$* c}
num contexto onde \mintinline{haskell}{a} e \mintinline{haskell}{b}
não são livres é:
\begin{minted}[escapeinside=**,mathescape=true]{coq}
  schm_arrow (schm_gen (0 nil))
      (schm_arrow (schm_gen (1 nil)) (schm_var (2 nil))).
\end{minted}

Os contextos, como numa tradicional implementação, são representados
como listas de tuplas. Neste caso são triplas de nome do
identificador, tipo da definição e o seu \textit{scheme} (ver Figura
\ref{fig_ctx}). Como apresentado no capítulo anterior, não há
condição de consistência nos contextos, o que não impacta em nada a
tradicional implementação do mesmo.  No entanto, até o momento não é
claro se isso impacta as provas das propriedades desejadas.

\begin{figure}[ht!]
  \caption{\label{fig_ctx}Formalização de contextos e \textit{typing}.}
\begin{minted}{coq}
Inductive kind_of_def : Type :=
| LET : kind_of_def
| LAM : kind_of_def.

Definition assump := (ty_name * kind_of_def * schm)%type.

Definition ctx := list assump.

Definition typing := (ty * ctx)%type.

Definition id_typing := (ty_name * typing)%type.

Definition inf_ctx := list id_typing.
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

As mônadas em linguagens funcionais puras permitem tratar computações
não triviais (com efeitos colaterais). No caso de algoritmos de
inferência, os quais necessitam de estados e exceções, mônadas toram a
implementação mais simples e mais legível. Coq, diferente de Haskell,
não conta por padrão com computações monádicas. Por outro lado, em Coq
é possível definir mônadas por meio de construtores de tipos. No caso do
MMo, define-se o construtor de tipo \mintinline{coq}{Infer} com as
operações \mintinline{coq}{ret} e \mintinline{coq}{bind}.
\mintinline{coq}{Ìnfer} esta representando uma computação de um
elemento de tipo \mintinline{coq}{A} que carrega um estado
\mintinline{coq}{tc_state} e pode conter ou não um valor devido ao
\mintinline{coq}{option}. A função \mintinline{coq}{failT} serve para
ser chamada em situações de falha, quando é necessário abortar a
inferência.

\begin{figure}[ht!]
  \caption{\label{fig_monad}Mônada de inferência.}
\begin{minted}{coq}
  Record tc_state := mkState {next_tvar : id}.
  
  Definition Infer (A : Type) := tc_state -> option (tc_state * A)%type.
  
  Definition ret (A : Type) (x : A) : Infer A := fun s => Some (s,x).
  
  Definition failT (A : Type) : Infer A := fun s => None.
  
  Definition bind (A B : Type) (c : Infer A)
                  (c' : A -> Infer B) : Infer B :=
    fun s =>
      match c s with
      | None => None
      | Some (s',v) => c' v s'            
      end.
  
  Notation "x <- c1 ; c2" := (bind c1 (fun x => c2)) 
                  (right associativity, at level 84, c1 at next level).
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

De maneira ortogonal ao objetivo desta dissertação, fez-se uma
reprodução da certificação do algoritmo \textit{W} de \cite{du:99},
mas somente no aspecto da consistência até o
momento\footnote{Disponível em \url{https://github.com/rafaelcgs10/W-in-Coq}.}.
Diferentemente desse, utilizaram-se funções monádicas as quais não causaram
complicações nas provas dos teoremas e lemas necessários. Portanto,
espera-se que o mesmo aconteça na formalização do MMo. Optou-se,
também, por utilizar tipos dependentes e automação de provas. Assim,
resultando numa verificação mais compacta que a original.

A computação do conjunto $\Omega$ é realizada por meio da aplicação de
\mintinline{coq}{foldM} (\mintinline{coq}{fold} monádico) na função
\mintinline{coq}{getTs} (ver Figura \ref{fig_getts}). Para verificar
se existe equivalência $\alpha$ entre um requerido e um inferido
quantifica-se ambos para compará-los sintaticamente.  O inferido
\mintinline{coq}{t'} é instanciado por \mintinline{coq}{supInst} e
colocado como primeiro elemento da dupla. Essa função é aplicada ao
longo de $\Sigma$ acumulando a lista \mintinline{coq}{tys_tys} que será o
$\Omega$ ao final.
\begin{figure}[ht!]
  \caption{\label{fig_getts}Função para computar o conjunto $\Omega$.}
\begin{minted}{coq}
  Definition getTs (ids : list id) (ids_tys : list (id_t * ty))
  (tys_tys : list (ty * ty)) (sigma : id_t * assump) :
  Infer (list (ty * ty)) :=
    match sigma with (n, (i, _, t)) =>
      let getT i sc :=
        match lookup_ids_tys i ids_tys with
        | Some t' =>
           let sc' := quantify (minus_list_id (ids_in_ty t') ids) t' in
           let sc := quantify (minus_list_id (ids_in_ty t') ids) t in
           if eq_schm_dec sc' sc then ret None else
             t'' <- supInst ids n t';
             ret (Some t'')
        | None => ret None
        end in 
      maybe_t <- getT i t;
      match maybe_t with
      | Some t' => ret ((t', t)::tys_tys)
      | None => ret tys_tys
      end
    end.
\end{minted}
\legend{Fonte: o autor. Adaptado de \cite{va:03}.}
\end{figure}

Para utilizar a unificação de \cite{ri:16} foi necessário escrever uma
interface para a função \mintinline{coq}{unify}, pois essa tem como
argumentos uma lista de identificadores de tipo, uma lista de tipos a
serem unificados e uma prova de que a lista é bem formada em relação
aos identificadores. O retorno de \mintinline{coq}{unify} é uma
substituição com uma prova que essa unifica os tipos na lista.
Na implementação tradicional realizada até o momento, não é
de importância utilizar tipos dependentes, portanto converte-se
\mintinline{coq}{unify} para \mintinline{coq}{unify_simple} com tipos
simples. Ainda, defini-se a função \mintinline{coq}{unify_listM} que
serve para unificar $\Omega$ de maneira monádica.

\section{Aspectos da terminação}
\label{sec_term}
Como Coq é uma linguagem de programação funcional total, então as
funções do MMo precisam ser pelo verificador de terminação, de maneira
que pode ser necessário construir provas de terminação. De todas as funções
do MMo, \mintinline[escapeinside=**,mathescape=true]{haskell}{unifyG}
é a única que não é de recursão primitiva e, portanto, é necessário
provar a sua parada.  Como critério de parada, provisório, utilizou-se
uma medida \mintinline{coq}{u_length} que calcula a efetividade de uma
substituição vinda da unificação do novo \mintinline{coq}{tsReq_tsInf}
($\Omega$).

\begin{figure}[ht!]
  \caption{\label{fig_getts}Função para computar o conjunto $\Omega$.}
\begin{minted}{coq}
Program Fixpoint unifyG (lbtvs : list id) (tsReq_tsInf : list (ty * ty))
  (is : list id_t) (infTypings : list typing)
  {measure (u_length tsReq_tsInf)} :
  Infer ((list (ty * ty)) * (list (id_t * typing))) :=
  let (l1, l2) := split tsReq_tsInf in
  s <- unify_listM l1 l2;
  let infTypings' := apply_subst_typings s infTypings in
  let all_g_i := List.concat (map snd infTypings') in
  tsReq_tsInf' <- foldM (getTs lbtvs (combine is (map fst infTypings')))
                  nil (zipNat all_g_i);
  let unem := u_length tsReq_tsInf in
  if Nat.eqb unem 0 then ret (tsReq_tsInf', combine is infTypings')
    else if circular_dep s then failT _ else
            (unifyG lbtvs tsReq_tsInf' is infTypings') _.
Next Obligation.
  Admitted.
\end{minted}
\legend{Fonte: o autor. Adaptado de \cite{va:03}.}
\end{figure}

Provar esse critério de parada provisório não foi possível até o momento.
Pois, é necessário demostrar que
\begin{minted}{coq}
u_length tsReq_tsInf' < u_length tsReq_tsInf
\end{minted}
mas não há óbvia evidência que isso seja verdadeiro e como prová-lo.
Isso não significa que o método de recursão bem-fundada não seja uma opção,
apenas que é desconhecida alguma propriedade da efetividade da substituição
que ajude nesta prova.

Outra abordagem é utilizar alguma forma de limite de iteração, como
apresentado na Seção Terminação em Coq. Seria necessário descobrir
como computar esse número ou, então, associar esse valor às regras de
inferência, assim a consistência ainda seria possível mesmo se esse valor for
desconhecido.

\section{Aspectos da consistência}
A formalização do algoritmo \textit{W} em Coq de \cite{du:99}
apresenta a prova da consistência e da completude com as regras
de Damas-Milner em sua versão \textit{syntax-directed}, proposta por
\cite{cl:86} (ver Figura \ref{fig_regras_sddm}). Nesta versão, as
regras de especialização $(spec)$ e generalização $(gen)$ são
fundidas, respectivamente, com as regras $(var)$ e $(let)$. As
regras são sempre da forma
\mintinline[escapeinside=**,mathescape=true]{haskell}{*$\Gamma \vdash $ *e* $:\tau$*},
onde $\tau$ é sempre um tipo simples e não um \textit{scheme}, mas os tipos
quantificados ainda podem ocorrer em contextos.
A vantagem é que esta representação determinista facilita aspectos das
provas, pois é possível unicamente determinar qual regra aplicar com
base na forma da expressão e, também, torna mais simples a
representação como um tipo indutivo em Coq. A equivalência desta
versão com a original declarativa e não-determinista também foi
formalizada em Coq por \cite{du:98}.

\begin{figure}[ht!]
  \caption{\label{fig_regras_sddm}Versão \textit{syntax-directed} das regras de inferência de Damas-Milner.}
  \begin{center}
    \begin{prooftree}
      \AxiomC{\mintinline{haskell}{x} $: \sigma \in \Gamma$}
      \AxiomC{$\sigma > \tau$}
      \RightLabel{$(var)$}
      \BinaryInfC{$\Gamma \vdash$ \mintinline{haskell}{x} $: \tau$}
    \end{prooftree}
  \end{center}

  \begin{center}
    \begin{minipage}{.5\textwidth}
      \begin{prooftree}
        \AxiomC{$\Gamma \vdash$
          \mintinline{haskell}{e}$:\tau \rightarrow \tau'$}
        \AxiomC{$\Gamma \vdash$ \mintinline{haskell}{e'}$:\tau$}
        \RightLabel{$(app)$} \BinaryInfC{$\Gamma \vdash$ \mintinline{haskell}{e
            e'}$: \tau'$}
      \end{prooftree}
    \end{minipage}
    \begin{minipage}{.4\textwidth}
      \begin{prooftree}
        \AxiomC{$\Gamma$,\mintinline{haskell}{x} $: \tau \vdash$ \mintinline{haskell}{e} $: \tau'$}
        \RightLabel{$(abs)$}
        \UnaryInfC{$\Gamma \vdash \lambda$\mintinline{haskell}{x.e} $: \tau \rightarrow \tau'$}
      \end{prooftree}
    \end{minipage}
  \end{center}

  \begin{center}
    \begin{prooftree}
      \AxiomC{$\Gamma \vdash$ \mintinline{haskell}{e} $:\tau$}
      \AxiomC{$\Gamma$, \mintinline{haskell}{x}$:$\mintinline{haskell}{gen}$(\Gamma, \tau) \vdash$ \mintinline{haskell}{e'} $: \tau'$}
      \RightLabel{$(let)$}
      \BinaryInfC{$\Gamma \vdash$ \mintinline{haskell}{let x = e in e'} $:\tau'$}
    \end{prooftree}
  \end{center}

  \begin{center}
    \begin{prooftree}
      \AxiomC{$\Gamma, $\mintinline{haskell}{x} $:\tau \vdash$
      \mintinline{haskell}{e} $:\tau$} \RightLabel{$(fix)$}
      \UnaryInfC{$\Gamma \vdash$ \mintinline{haskell}{fix x.e} $:\tau$}
    \end{prooftree}
  \end{center}
\legend{Fonte: o autor. Adaptado de \cite{cl:86}.}
\end{figure}

Pelos motivos apresentados acima, a prova da consistência do MMo é
mais facilmente realizável em relação à versão
\textit{syntax-directed} de Milner-Mycroft. 

\textit{syntax-directed} (ver Figura \ref{fix_mm_direct}).
Em \cite{he:93} há uma demostração que Milner-Mycroft na versão
\textit{syntax-directed} tem o mesmo poder que a versão original
declarativa.
\begin{figure}[ht!]
  \caption{\label{fix_mm_direct}Versão \textit{syntax-directed} da regra $fix+$.}
  \begin{center}
    \begin{prooftree}
      \AxiomC{$\Gamma, $\mintinline{haskell}{x} $:$\mintinline{haskell}{gen}$(\Gamma, \tau) \vdash$ \mintinline{haskell}{e} $:\tau$}
      \RightLabel{$(fix+)$}
      \UnaryInfC{$\Gamma \vdash$ \mintinline{haskell}{fix x.e} $:\tau$}
    \end{prooftree}
  \end{center}
\legend{Fonte: o autor.}
\end{figure}

A formalização em Coq das regras de inferência de Milner-Mycroft
em \textit{syntax-directed} é feita de maneira bastante óbvia.
A função de generalização é a \mintinline{coq}{gen_ty} e
\mintinline{coq}{is_schm_instance} é um predicado que garante
que \mintinline{coq}{tau} é uma instância de
\mintinline{coq}{sigma}\footnote{Como esta formalização é baseada em \cite{du:99},
  então este detalhe e diversos outros podem ser vistos no mesmo.}.
Como as regras são sempre da forma
\mintinline[escapeinside=**,mathescape=true]{haskell}{*$\Gamma \vdash $ *e* $:\tau$*},
então fica trivial representar com um tipo indutivo de assinatura
\mintinline{coq}{ctx -> term -> ty -> Prop}. Definindo, assim, uma
relação indutiva.
\begin{figure}[ht!]
  \caption{\label{fig_hastype}Tipo indutivo das regras de inferência de Milner-Mycroft em
    \textit{syntax-directed}.}
\begin{minted}{coq}
Inductive has_type : ctx -> term -> ty -> Prop :=
| const_ht : forall i G, has_type G const_t (con i)
| var_ht : forall x G sigma tau, in_ctx x G = Some sigma ->
                             is_schm_instance tau sigma ->
                             has_type G (var_t x) tau
| lam_ht : forall x G tau tau' e,
                             has_type ((x, ty_to_schm tau)::G) e tau' ->
                             has_type G (lam_t x e) (arrow tau tau')
| app_ht : forall G tau tau' l rho, has_type G l (arrow tau tau') ->
                             has_type G rho tau ->
                             has_type G (app_t l rho) tau'
| let_ht : forall G x e e' tau tau', has_type G e tau ->
                             has_type ((x, gen_ty tau G)::G) e' tau' ->
                             has_type G (let_t x e e') tau'
| fix_ht : forall , has_type ((x, gen_ty tau G)::G) e tau ->
                             has_type G (fix_t x e) tau.
\end{minted}
\legend{Fonte: o autor.}
\end{figure}

Este tipo indutivo é uma certificação que pode ser diretamente utilizada
num tipo dependente no MMo:
\begin{minted}{coq}
Definition infer_dep : forall (e : term) (G : ctx),
    Infer ({tau : ty & {g : ctx | has_type (apply_subst_ctx s G) e tau}}),
\end{minted}
o que reduz alguns passos da prova de certificação (ex: não há
indução), caso fosse realizada posteriormente. Observa-se que o mônada aceita
como argumento um tipo dependente (o existencial \mintinline{coq}{sigT}).

\subsection{Estabilidade da substituição}
O lema da estabilidade da substituição é uma propriedade clássica em
sistemas de tipos e é fundamental na prova da consistência. Em suma,
se é verdade que
\mintinline[escapeinside=**,mathescape=true]{haskell}{*$\Gamma \vdash
  $ *e* $:\tau$*}, então para qualquer substituição $\mathbb{S}$
tem-se
\mintinline[escapeinside=**,mathescape=true]{haskell}{*$\mathbb{S}
  \Gamma \vdash $ *e* $:\mathbb{S} \tau$*}.  Logo, se $tau$ é um tipo
possível para \mintinline{haskell}{e} sobre $\Gamma$, então pode-se
obter outro tipo para \mintinline{haskell}{e} aplicando $\mathbb{S}$
em $\tau$ e $\Gamma$.  Na formalização o lema é dado por

\begin{minted}{coq}
Lemma has_type_is_stable_under_substitution : forall e s G tau,
  has_type G e tau ->
  has_type (apply_subst_ctx s G) e (apply_subst s tau).
\end{minted}

Não há grandes dificuldades nos casos monomórficos deste lema.
Nesta formalização, além do caso polimórfico do \mintinline{coq}{let_ht}
há também o caso do \mintinline{coq}{fix_ht}. Detalhes de como provar o
caso do \mintinline{coq}{let_ht} são detalhados em \cite{du:99}. Espera-se que
o caso do \mintinline{coq}{fix_ht} não seja mais difícil. Um aspecto importante
deste lema é ser uma propriedade estritamente do sistema de tipos e não envolver
o algoritmo utilizado.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
